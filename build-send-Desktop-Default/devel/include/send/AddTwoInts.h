// Generated by gencpp from file send/AddTwoInts.msg
// DO NOT EDIT!


#ifndef SEND_MESSAGE_ADDTWOINTS_H
#define SEND_MESSAGE_ADDTWOINTS_H

#include <ros/service_traits.h>


#include <send/AddTwoIntsRequest.h>
#include <send/AddTwoIntsResponse.h>


namespace send
{

struct AddTwoInts
{

typedef AddTwoIntsRequest Request;
typedef AddTwoIntsResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct AddTwoInts
} // namespace send


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::send::AddTwoInts > {
  static const char* value()
  {
    return "6a2e34150c00229791cc89ff309fff21";
  }

  static const char* value(const ::send::AddTwoInts&) { return value(); }
};

template<>
struct DataType< ::send::AddTwoInts > {
  static const char* value()
  {
    return "send/AddTwoInts";
  }

  static const char* value(const ::send::AddTwoInts&) { return value(); }
};


// service_traits::MD5Sum< ::send::AddTwoIntsRequest> should match 
// service_traits::MD5Sum< ::send::AddTwoInts > 
template<>
struct MD5Sum< ::send::AddTwoIntsRequest>
{
  static const char* value()
  {
    return MD5Sum< ::send::AddTwoInts >::value();
  }
  static const char* value(const ::send::AddTwoIntsRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::send::AddTwoIntsRequest> should match 
// service_traits::DataType< ::send::AddTwoInts > 
template<>
struct DataType< ::send::AddTwoIntsRequest>
{
  static const char* value()
  {
    return DataType< ::send::AddTwoInts >::value();
  }
  static const char* value(const ::send::AddTwoIntsRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::send::AddTwoIntsResponse> should match 
// service_traits::MD5Sum< ::send::AddTwoInts > 
template<>
struct MD5Sum< ::send::AddTwoIntsResponse>
{
  static const char* value()
  {
    return MD5Sum< ::send::AddTwoInts >::value();
  }
  static const char* value(const ::send::AddTwoIntsResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::send::AddTwoIntsResponse> should match 
// service_traits::DataType< ::send::AddTwoInts > 
template<>
struct DataType< ::send::AddTwoIntsResponse>
{
  static const char* value()
  {
    return DataType< ::send::AddTwoInts >::value();
  }
  static const char* value(const ::send::AddTwoIntsResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // SEND_MESSAGE_ADDTWOINTS_H
