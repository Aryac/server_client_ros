#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/arya/catkin_ws/src/build-send-Desktop-Default/devel:/opt/ros/melodic:/home/arya/catkin_ws/devel"
export LD_LIBRARY_PATH="/home/arya/catkin_ws/src/build-send-Desktop-Default/devel/lib:/opt/ros/melodic/lib:/home/arya/catkin_ws/devel/lib"
export PKG_CONFIG_PATH="/home/arya/catkin_ws/src/build-send-Desktop-Default/devel/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig:/home/arya/catkin_ws/devel/lib/pkgconfig"
export PWD="/home/arya/catkin_ws/src/build-send-Desktop-Default"
export PYTHONPATH="/home/arya/catkin_ws/src/build-send-Desktop-Default/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/home/arya/catkin_ws/devel/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/arya/catkin_ws/src/build-send-Desktop-Default/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/arya/catkin_ws/src/send:/opt/ros/melodic/share:/home/arya/catkin_ws/src"