#include "ros/ros.h"
#include "std_msgs/String.h"
#include <iostream>
#include "subscriber/listener.h"
#include "string"
using namespace std_msgs;
using namespace std;

int main(int argc, char **argv)
{

    ros::init(argc, argv, "listener");
    listener listener1 ;
    ros::spin();
    return 0;
}
