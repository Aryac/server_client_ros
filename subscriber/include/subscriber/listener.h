#ifndef LISTENER_H
#define LISTENER_H
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"
using namespace std;
class listener
{
private:

    ros::Subscriber sub_;

    ros::NodeHandle *node_;


public:

  listener();
  ~listener();

void Callback(const std_msgs::String::ConstPtr &msg);

};

#endif //talker_h
