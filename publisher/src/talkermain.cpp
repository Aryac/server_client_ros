#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <iostream>
#include "publisher/talker.h"

using namespace std;

int main(int argc, char **argv)
{


    ros::init(argc, argv, "talker");

    ros::spinOnce();

    talker talk;

    talk.talkerfunc();

}
