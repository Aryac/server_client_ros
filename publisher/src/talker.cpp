#include "ros/ros.h"
#include "std_msgs/String.h"
#include <iostream>
#include <sstream>
#include "publisher/talker.h"
using namespace std ;

talker :: talker()

{
    node_ = new ros::NodeHandle ;


    pub_ = node_->advertise<std_msgs::String>("message", 1000);
}

void talker ::talkerfunc()

{


    ros::Rate loop_rate(10);

    while (ros::ok())
    {

        std_msgs::String msg;

        std::stringstream send_text;

        send_text << " *** Im Send Message *** " << count_;

        msg.data = send_text.str();

        ROS_INFO("%s", msg.data.c_str());

        pub_.publish(msg);

        loop_rate.sleep();

        ++count_;
    }
}
