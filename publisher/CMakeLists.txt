cmake_minimum_required(VERSION 2.8.3)

set(CMAKE_PREFIX_PATH "/opt/ros/melodic" "/home/arya/catkin_ws/devel")

include_directories(include ${PROJECT_SOURCE_DIR}/build)

project(publisher)

## Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp rospy std_msgs  genmsg)

## Declare ROS messages and services
add_message_files(FILES Num.msg)
add_service_files(FILES AddTwoInts.srv)

## Generate added messages and services
generate_messages(DEPENDENCIES std_msgs)

## Declare a catkin package
catkin_package(CATKIN_DEPENDS message_runtime )

## Build talker

include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(talker src/talkermain.cpp src/talker.cpp include/publisher/talker.h)
target_link_libraries(talker ${catkin_LIBRARIES})
add_dependencies(talker publisher_generate_messages_cpp)

