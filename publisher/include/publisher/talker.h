#ifndef TALKER_H
#define TALKER_H
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"
using namespace std;
class talker
{
private:

    ros::Publisher pub_ ;

    ros::NodeHandle *node_;

    int count_ = 0;

public:

  talker();

  void talkerfunc() ;

};

#endif //talker_h
