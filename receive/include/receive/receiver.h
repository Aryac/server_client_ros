#ifndef RECEIVER_H
#define RECEIVER_H
#include <iostream>
#include "ros/ros.h"
#include "receive/AddTwoInts.h"
#include "send/AddTwoInts.h"
#include "std_msgs/String.h"

using namespace std;

class receiver
{
private:

    ros::NodeHandle *node_;

    ros::ServiceClient client ;

    int sum = 0 ;

    receive::AddTwoInts srv ;


public:

    receiver();

    long int sendfunc(char **argv);

};

#endif //receiver_h
