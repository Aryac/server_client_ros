﻿#include "ros/ros.h"
#include "receive/receiver.h"
#include <cstdlib>
#include "receive/AddTwoInts.h"
#include "send/AddTwoInts.h"
#include <iostream>
using namespace std;

receiver::receiver()

{

    node_ = new ros::NodeHandle ;

    client = node_->serviceClient<receive::AddTwoInts>("add_two_ints");


}


long int receiver ::sendfunc(char **argv)

{

    srv.request.a = atoll(argv[1]);

    srv.request.b = atoll(argv[2]);

    if (client.call(srv))
    {
        ROS_INFO("Sum: %ld", (long int) srv.response.sum);
    }
    else
    {
        ROS_ERROR("Failed to call service add_two_ints");

        return 1;
    }

}


