﻿#ifndef SENDER_H
#define SENDER_H
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "send/AddTwoInts.h"


using namespace std;

class sender
        
{
    
private:

    ros::NodeHandle *node_;

    int sum = 0 ;



public:

    sender();

    bool add(send::AddTwoInts::Request  &req,send::AddTwoInts::Response &res);

    ros::ServiceServer service;
};

#endif //sender_h
