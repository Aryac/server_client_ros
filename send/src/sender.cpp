#include "ros/ros.h"
#include "std_msgs/String.h"
#include <iostream>
#include "send/AddTwoInts.h"
#include "send/sender.h"
using namespace std;

sender::sender()

{

    node_ = new ros::NodeHandle ;

    service = node_->advertiseService("add_two_ints", &sender::add,this);

    ROS_INFO("Ready to add two ints.");

}

bool sender::add(send::AddTwoInts::Request  &req, send::AddTwoInts::Response &res)

{

  res.sum = req.a + req.b;

  ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);

  ROS_INFO("sending back response: [%ld]", (long int)res.sum);

  return true;

}

