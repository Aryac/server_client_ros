#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <iostream>
#include "send/sender.h"

using namespace std;


int main(int argc, char **argv)

{
    ros::init(argc, argv, "add_two_ints_server");

    sender sendreceive ;

    ros::spin();

    return 0;
}
